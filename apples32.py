# -*- coding: utf-8 -*-

# -*- coding: cp1251 -*-
#===================================================
WORDLIST_FILENAME = "apples.txt"
MAX_SCORE_COUNT = 1
Data = {}
#===================================================
def load():
    #заполняем базу (словарь)
    print ("Loading...")
    file = open(WORDLIST_FILENAME, 'r')
    contents_list = file.readlines()
    for i in range(len(contents_list)-1):
        res = ''
        res2 = ''
        for q in contents_list[i]:
            if (q!='\n'):
                res+=q
        for w in range(len(res)):
            if res[w]!=' ':
                res2+=res[w]
            elif res[w+1]!=' ':
                res2+=res[w]                
        Data[i]=res2.split(' ')
        res = ''
        res2 = ''
    file.close()
    print ('Loading completed')
    print
    return 
#===================================================
def showMeBetterHelp():
    print ('//***************************//')
    betterL = []
    for q in range(1,len(Data[0])):
        print ('// '+str(Data[0][q])+': ')
        for w in range(1,len(Data)-1):
            betterL.append(Data[w][q])
        betterL = list(set(betterL))
        for r in betterL:
            print ('   >>> '+str(r))
        betterL = []
    print ('//***************************//')
    print
#===================================================
def loadList(L):
    #создаем список ключевых слов
    for i in Data:
        for q in range(len(Data[i])):
            L.append(Data[i][q])
    L = list(set(L))
    return 

def sToList(s,sList):
    sList = s.split(' ')
    
def printList(L):
    #для вызова печати ключевых слов
    print ('==>Ключевые слова<==')
    print ('--------------------')
    for i in L:
        print ("-> "+ i)
    print ('--------------------') 

def clearScore(ScoreList):
    for i in range(len(ScoreList)):
        ScoreList[i] = 0
#===================================================
def findWordInBase(word, ScoreList):
    for i in Data:
        for t in Data[i]:
            if (t == word):
                ScoreList[i]+=1
                
def findBestResult(sList, L, ScoreList):
    for i in sList:
        for word in L:
            if (i == word):
                findWordInBase(i, ScoreList)
#===================================================
def getAnswer(sList, L):
    #ответ за запрос
    ScoreList = [] #ведение счета - список
    for i in range(len(Data)):
        ScoreList.append(0)
    findBestResult(sList, L, ScoreList)

    MaxScore = max(ScoreList)
    MaxScoreCount = 0
    Avalible = 0
    for i in range(len(ScoreList)):
        if ScoreList[i]==MaxScore:
            Avalible = i
            MaxScoreCount+=1
    if MaxScoreCount>MAX_SCORE_COUNT:
        print ("Рекомендуется ввести дополнительные критерии поиска")
        print ("Предворительный результат: "+ str(Data[i][0]))
    else:
        print ('==>Вам подойдёт сорт - '+ str(Data[Avalible][0]))
#===================================================
def searchKeyWord(spisok,L):
    counter = 0
    for i in spisok:
        for q in L:
            if i==q:
                counter+=1
    if (counter<1):
        print ('!!!!!!!!!!!!!нет совпадений')
        return False
    return True
#===================================================
def startprogram():
    print ("==================")
    print ("|Добро пожаловать|")
    print ("==================")
    print
	
    L = []      #список ключевых слов
    sList = []  #список введенных слов для поиска по базе
    ScoreList = [] #ведение счета - список
    for i in range(len(Data)):
        ScoreList.append(0)
        
    loadList(L)
    L = list(set(L)) #удаление повторов

    while True:
        s = input("Введите команду или критерии поиска\n- ")
        if (s == 'exit') or (s == 'выход'):
            return
        elif (s == 'лист')or(s == 'help')or(s == 'list'):
            printList(L)
        elif s == '':
            print ('empty')
        elif s == 'better help':
            showMeBetterHelp()
        elif (s == 'clear') or (s == 'очистить'):
            sList = []
            print ('Список ключевых слов очищен')
        elif (s == 'show'):
            getAnswer(sList, L)
        else:
            if searchKeyWord(s.split(' '),L):
                sList += s.split(' ')
                sList = list(set(sList))
                findBestResult(sList, L, ScoreList)
                print (ScoreList)
                getAnswer(sList, L)
                clearScore(ScoreList)
#===================================================            
def start():
    load()
    startprogram()

start()
