# -*- coding: cp1251 -*-
#===================================================

WORDLIST_FILENAME = "betterApples.txt"
Data = []
#===================================================
def load():
    '''
    Filling Data from file WORDLIST_FILENAME
	
    >>> Data
    [['sort', 'taste', 'size', 'color', 'country'],
    [...],[...],[...],
    ...]
    '''
    #�������� ��������� ����
    if len(Data)>0:
        for i in range(len(Data)):
            del Data[0]
    print ("Loading...")
    file = open(WORDLIST_FILENAME, 'r')
    contents_list = file.readlines()
    for i in range(len(contents_list)):
        res = ''#����� ���������� ������
        res2 = ''
        buff = []
        for q in contents_list[i]:
            if (q!='\n'):
                res+=q # ���� �� ����� ������, ���������� � ����������
        for w in range(len(res)-1):
            if res[w]!=' ':
                res2+=res[w]
            elif res[w+1]!=' ':
                res2+=res[w]
        buff = res2.split(' ')
        Data.append(buff)
        buff = []
        res = ''
        res2 = ''
    file.close()
    print ('Loading completed')
    print
    return


def showSorts():
    print ('������ ������:')
    for i in range(1, len(Data)):
        print ('|  '+str(Data[i][0]))
    print


def showAll():
    print ('������ ������:')
    maxLen = []
    for i in range(len(Data[0])):
        maxLen.append(0)
        
    for i in range (len(Data)):
        for q in range(len(Data[i])):
            if len(Data[i][q])>maxLen[q]:
                maxLen[q]=len(Data[i][q])
                
    for i in range(len(Data)):
        l = len(Data[i])
        out = ''
        for q in range(l):
            if len(Data[i][q])<maxLen[q]:
                out+=Data[i][q]
                out+=(' '*(maxLen[q]-len(Data[i][q])+2))
            else:    
                out+=Data[i][q]
                out+='  '
        print (out)
    print    


def showAllCheck():
    maxLen = []
    for i in range(len(Data[0])):#��� ����� ���������...
        maxLen.append(0)
        
    for i in range (len(Data)):
        for q in range(len(Data[i])):
            if len(Data[i][q])>maxLen[q]:
                maxLen[q]=len(Data[i][q])
    print ('�� � �������')

    
def addNewSort():
    s = raw_input('�������' + str(Data[0]) +'... \n')
    Data.append(s.split(' '))


def delSort():
    s = raw_input('������� �������� �����, ������� ���������� �������\n')
    flag = False
    for i in range(len(Data)):
        #print ('�������� '+str(Data[i][0]))
        if Data[i][0] == s:
            print '���� ������'
            flag = True
            del Data[i]
            print '������ � ����� ������� �������'
            return
    if flag==False:
        print '���� �� ������'


def saveNew():
    print ('Saving...')
    file = open(WORDLIST_FILENAME, 'w')
    maxLen = []
    for i in range(len(Data[0])):
        maxLen.append(0)
        
    for i in range (len(Data)):
        for q in range(len(Data[i])):
            if len(Data[i][q])>maxLen[q]:
                maxLen[q]=len(Data[i][q])
                
    for i in range(len(Data)):
        l = len(Data[i])
        out = ''
        for q in range(l):
            if len(Data[i][q])<maxLen[q]:
                out+=Data[i][q]
                out+=(' '*(maxLen[q]-len(Data[i][q])+2))
            else:    
                out+=Data[i][q]
                out+='  '
        file.write(out+'\n')
    file.close()
    print ('Saving completed')

    
def start():
    print ("==================")
    print ("|����� ����������|")
    print ("==================")
    print

    load()
    
    while True:
        s = raw_input("������� ������� \n- ")
        if (s == 'exit') or (s == '�����'):
            return

        elif (s.lower() == '������'):
            showSorts()
            
        elif s == '':
            print ('EMPTY')
            
        elif (s.lower() == '��������'):
            print '��������� �� ���������� ������ ����� � ��� ��������� ����������'
            addNewSort()
            
        elif (s.lower() == '�������'):
            print ('������� ��������� �� �������� ������-�� �����')
            delSort()
            
        elif (s.lower() == 'show'):
            showAll()

        elif (s.lower() == 'qw'):
            showAllCheck()            
            
        elif (s.lower() == 'save'):
            saveNew()   
        else:
            print '������� �� ����������' 
