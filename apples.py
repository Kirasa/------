# -*- coding: cp1251 -*-
#===================================================
WORDLIST_FILENAME = "betterApples.txt"
INFO_FILENAME = "INFO.txt"
MAX_SCORE_COUNT = 1
Data = {}
#===================================================
def load():
    '''
    Filling Data from file WORDLIST_FILENAME
	
    >>> Data
    {0: ['sort', 'taste', 'size', 'color', 'country'],
    1: ...
    '''
    print ("Loading...")
    file = open(WORDLIST_FILENAME, 'r')
    contents_list = file.readlines()
    for i in range(len(contents_list)-1):
        res = ''
        res2 = ''
        for q in contents_list[i]:
            if (q!='\n'):
                res+=q
        for w in range(len(res)-1):
            if res[w]!=' ':
                res2+=res[w]
            elif res[w+1]!=' ':
                res2+=res[w]                
        Data[i]=res2.split(' ')
        res = ''
        res2 = ''
    file.close()
    print ('Loading completed')
    print
    return


def showMeBetterHelp():
    '''
    Show a list of keywords, divided into categories.
	
    '''
    print ('//***************************//')
    betterL = []
    for q in range(1,len(Data[0])):
        print ('// '+str(Data[0][q])+': ')
        for w in range(1,len(Data)):
            betterL.append(Data[w][q])
        betterL = list(set(betterL))
        for r in betterL:
            print ('   >>> '+str(r))
        betterL = []
    print ('//***************************//')
    print


def loadList(L):
    '''
    Generating keywords in list L 
    '''
    for i in Data:
        for q in range(len(Data[i])):
            L.append(Data[i][q])
    L = list(set(L))
    return

    
def printList():
    '''
    Print commands
    '''
    print ('==>�������<==')
    print ('--------------------')
    print ('>>> exit/�����')
    print ('>>> help')
    print ('>>> list')
    print ('>>> better help')
    print ('>>> clear/��������')
    print ('>>> show')
    print ('--------------------') 


def clearScore(ScoreList):
    '''
    Clearing score list
    '''
    for i in range(len(ScoreList)):
        ScoreList[i] = 0


def findWordInBase(word, ScoreList):
    '''
    ����� ����������, ��������� ������� �������� (+1)
    '''
    for i in Data:
        for t in Data[i]:
            if (t == word):
                ScoreList[i]+=1

def findWordInBase2(word, ScoreList):
    '''
    ����� ����������, ��������� ������� �������� (+1)
    '''
    for i in Data:
        for t in Data[i]:
            if (t == word):
                ScoreList[i]-=1

                
def findBestResult(sList, L, ScoreList):
    
    for i in sList:
        if i[0]=='-':
            for word in L:
                if (i[1:]==word):
                    findWordInBase2(i[1:], ScoreList)
        else:    
            for word in L:
                if (i == word):
                    findWordInBase(i, ScoreList)
                

def getAnswer(sList, L):
    #����� �� ������
    ScoreList = [] #������� ����� - ������
    for i in range(len(Data)):
        ScoreList.append(0)
    findBestResult(sList, L, ScoreList)

    MaxScore = max(ScoreList)
    MaxScoreCount = 0
    Avalible = 0
    for i in range(len(ScoreList)):
        if ScoreList[i]==MaxScore:
            Avalible = i
            MaxScoreCount+=1
    if MaxScoreCount>MAX_SCORE_COUNT:
        print ("������������� ������ �������������� �������� ������")
        print ("��������������� ���������: "+ str(Data[i][0]))
    else:
        print ('==>��� ������� ���� - '+ str(Data[Avalible][0]))


def searchKeyWord(spisok,L):
    counter = 0
    for i in spisok:
        for q in L:
            if i==q:
                counter+=1
    if (counter<1):
        print ('���������� �� �������')
        return False
    return True


def showList(sList):
    if len(sList)==0:
        print ('������ ����')
    for i in sList:
        print ('> '+str(i))


def tryShowSort(s):
    for i in range(len(Data)):
        if (s==Data[i][0]):
            file = open(INFO_FILENAME, 'r')
            listInfo = file.readlines()
            print (str(listInfo[i]))
            file.close()
            break
		
def startprogram():
    print ("==================")
    print ("|����� ����������|")
    print ("==================")
    print
	
    L = []          #������ �������� ����
    sList = []      #������ ��������� ���� ��� ������ �� ����
    ScoreList = []  #������� ����� - ������
    for i in range(len(Data)):
        ScoreList.append(0)
        
    loadList(L)
    L = list(set(L)) #�������� ��������

    while True:
        s = raw_input("������� ������� ��� �������� ������\n- ")
        if (s == 'exit') or (s == '�����'):
            return
        
        elif (s == 'help'):
            printList()

        elif (s == 'list'):
            showList(sList)
            
        elif s == '':
            print ('empty')
            
        elif s == 'better help':
            showMeBetterHelp()
            
        elif (s == 'clear') or (s == '��������'):
            sList = []
            print ('������ �������� ���� ������')
            
        elif (s == 'show'):
            getAnswer(sList, L)
        
        elif (s[0]=='!'):
            tryShowSort(s[1:])
            print '����� �������� �����\n'
            s = ''
        else:
            if s[0]=='-':
                print ('����� ��������')
                if searchKeyWord(s[1:].split(' '),L):
                    sList += s.split(' ')
                    sList = list(set(sList))
                    findBestResult(sList, L, ScoreList)
                    print ScoreList # ����� �����
                    getAnswer(sList, L)
                    clearScore(ScoreList)
                    
            elif searchKeyWord(s.split(' '),L):
                sList += s.split(' ')
                sList = list(set(sList))
                findBestResult(sList, L, ScoreList)
                print ScoreList # ����� �����
                getAnswer(sList, L)
                clearScore(ScoreList)

#===================================================            
def start():
    load()
    startprogram()

start()
